package au.com.traverse.registry.model

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public const val CURRENT_SCHEMA_LEVEL = 2;

data class ApplianceRegistry(
    val registryName: String,
    val lastUpdated: ZonedDateTime,
    val appliances: List<Appliance>,
    val schemaLevel: Int ?= CURRENT_SCHEMA_LEVEL
)
{
}