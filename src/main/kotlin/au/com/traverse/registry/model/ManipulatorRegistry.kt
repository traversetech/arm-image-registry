package au.com.traverse.registry.model

import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;

data class ManipulatorRegistry(
    val registryName: String? = null,
    val baseHref: String? = null,
    val lastUpdated: LocalDateTime? = null,
    val manipulators: List<Manipulator>? = null
)
{
    
}