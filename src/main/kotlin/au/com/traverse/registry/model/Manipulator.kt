package au.com.traverse.registry.model

import kotlin.text.String;

data class Manipulator(
    var id : String? = null,
    var appliances: List<String> ? = null,
    var description: String ?= null,
    var infourl: String ?= null,
    var hardware: List<String> ?= null,
    var author: String ?= null,
    var file: String ?= null,
    var shasum: String ?= null,
    var fixup: Boolean ?= false,
    var provisioner: Boolean ?= false
) {

}