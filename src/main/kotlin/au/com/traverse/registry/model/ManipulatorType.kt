package au.com.traverse.registry.model

enum class ManipulatorType {
    FIXUP,
    PROVISIONER
}