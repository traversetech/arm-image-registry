package au.com.traverse.registry.model

import kotlin.text.String;
import kotlinx.serialization.Serializable;

@Serializable
data class Appliance(
    val id: String,
    val name: String,
    val description: String,
    val maintainer: String,
    var download: String,
    var link: String,
    val checksum: String,
    val format: String? = "qcow2",
    val version: String,
    val mindisk: Int,
    val minram: Int,
    var supported_hardware: List<String>  ?= null,
    var incompat_hardware: List<String>  ?= null,
    val cloudinit: Boolean,
    var properties: Map<String,String> ?= null,
)
{

}