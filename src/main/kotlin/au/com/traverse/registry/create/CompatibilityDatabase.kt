package au.com.traverse.registry.create

import java.io.FileNotFoundException;
import java.io.File;

import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

class CompatibilityDatabase(
        val hardwareIncompatList: Map<String, List<String>>,
        val perApplianceProperties: Map<String, Map<String,String>>
) {

    fun checkForHardwareIncompatMatch(applianceId: String): List<String> {
        var incompatHardware = mutableListOf<String>()

        for ((hardwareId, hardwareIncompatList) in hardwareIncompatList) {
            for (incompatRegex in hardwareIncompatList) {
                val isIncompatMatch = Pattern.matches(incompatRegex, applianceId)
                if (isIncompatMatch) {
                    incompatHardware.add(hardwareId)
                }
            }
        }
        return incompatHardware
    }

    fun getPropertiesForAppliance(applianceId: String): Map<String, String> {
        for((applianceIdRegex, applianceProperties) in perApplianceProperties) {
            val isApplianceMatch = Pattern.matches(applianceIdRegex, applianceId);
            if (isApplianceMatch) {
                /* for((propertyName, propertyValue) in applianceProperties) {
                    properties.put(propertyName, propertyValue.toString());
                } */
                return applianceProperties;
            }
        }
        return mutableMapOf<String,String>();
    }
}

fun loadCompatibilityDatabaseFromJSON(jsonRoot: ObjectNode): CompatibilityDatabase {
    val incompatMap = mutableMapOf<String,List<String>>();
    val perApplianceProperties = mutableMapOf<String,Map<String,String>>();

    val incompatibilities = jsonRoot.get("incompatibilities");
    if (incompatibilities != null && incompatibilities.getNodeType() == JsonNodeType.OBJECT) {
        val incompatTree = incompatibilities as ObjectNode;
        for (hardwareId in incompatTree.fieldNames()) {
            val hardwareIncompatArray = incompatTree.get(hardwareId) as ArrayNode;
            val incompatibleRegexes = mutableListOf<String>();
            for(arrayValueNode in hardwareIncompatArray.elements()) {
                val valueString = arrayValueNode.asText();
                incompatibleRegexes.add(valueString);
            }
            incompatMap.put(hardwareId, incompatibleRegexes);
        }
    }

    val propertiesNode = jsonRoot.get("properties");
    if (propertiesNode != null && propertiesNode.getNodeType() == JsonNodeType.OBJECT) {
        val propertiesTree = propertiesNode as ObjectNode;
        for (applianceMatchRegex in propertiesTree.fieldNames()) {
            val thisApplianceProperties = mutableMapOf<String,String>();
            val thisAppliancePropertiesNode = propertiesTree.get(applianceMatchRegex) as ObjectNode;
            for(appliancePropertyName in thisAppliancePropertiesNode.fieldNames()) {
                thisApplianceProperties.put(appliancePropertyName,
                thisAppliancePropertiesNode.get(appliancePropertyName).asText());
            }
            perApplianceProperties.put(applianceMatchRegex, thisApplianceProperties);
        }
    }
    return CompatibilityDatabase(incompatMap, perApplianceProperties);
}

fun loadCompatibilityDatabaseFromFile(incompatFilePath: String): CompatibilityDatabase {
    val incompatFile = File(incompatFilePath)
    if (!incompatFile.exists()) {
        throw FileNotFoundException("Incompatibility list file not found at path: " + incompatFile.getPath());
    }
    val objMapper = ObjectMapper();
    val incompatTree = objMapper.readTree(incompatFile) as ObjectNode;
    
    return loadCompatibilityDatabaseFromJSON(incompatTree);
}