package au.com.traverse.registry.create;

import org.apache.hc.client5.http.impl.classic.HttpClients;

import au.com.traverse.registry.distributions.*;
import au.com.traverse.registry.model.*;

import kotlinx.serialization.*
import kotlinx.serialization.builtins.*;
import kotlinx.serialization.json.*

import java.io.File;
import java.io.FileNotFoundException

import java.nio.file.Files;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.core5.http.io.entity.EntityUtils;

import org.jsoup.nodes.Document;

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper;
import com.fasterxml.jackson.module.kotlin.kotlinModule;

@kotlinx.serialization.Serializable
class ExternalApplianceProperties(
    val path: String,
    @SerialName ("override_env")
    val overrideEnvVar: String,
    val file: String
) {};

@kotlinx.serialization.Serializable
class ExternalApplianceConfig(
    val registryName: String,
    val appliances: Map<String,ExternalApplianceProperties>
) {};

class CreateFromExternal {
    var mapper: ObjectMapper;
    var httpClient: HttpClient;

    init {
        mapper = ObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.registerModule(JavaTimeModule());
        mapper.registerModule(kotlinModule());

        httpClient = HttpClients.custom()
        .setUserAgent(USER_AGENT)
        .build();
    }

    fun loadApplianceDataFromURL(httpClient: HttpClient, mapper: ObjectMapper, url: String): Appliance {
        val applianceResponse = httpClient.execute(HttpGet(url)) as ClassicHttpResponse;
        val applianceEntity = applianceResponse.getEntity();

        var appliance = mapper.readValue(applianceEntity.getContent(), Appliance::class.java);
        EntityUtils.consumeQuietly(applianceEntity);
        return appliance;
    }

    fun loadFromConfig(filePath:  String): ExternalApplianceConfig {
        val jsonEncoder = Json { prettyPrint = true; ignoreUnknownKeys = true };
        val configFile = File(filePath);
        val configBytes: ByteArray = Files.readAllBytes(configFile.toPath());
        val configString = String(configBytes);

        var externalConfig: ExternalApplianceConfig = jsonEncoder.decodeFromString(ExternalApplianceConfig.serializer(), configString);

        return externalConfig;
    }

    fun getAppliances(config: ExternalApplianceConfig): List<Appliance> {
        val appliances = mutableListOf<Appliance>();

        for((externalApplianceId, externalApplianceProperties) in config.appliances) {
            println("${externalApplianceId}: ${externalApplianceProperties.path} ${externalApplianceProperties.overrideEnvVar}");
            var overrideEnvVariable = System.getenv(externalApplianceProperties.overrideEnvVar);
            var appliancePubPath : String;
            if (overrideEnvVariable != null) {
                println("${externalApplianceId}: overridden to ${overrideEnvVariable}")
                appliancePubPath = overrideEnvVariable;
            } else {
                appliancePubPath = externalApplianceProperties.path;
            }

            val getUrl = appliancePubPath + externalApplianceProperties.file;
            println("Fetching from ${getUrl}");
    
            val appliance = loadApplianceDataFromURL(httpClient, mapper, getUrl);
            if (!appliance.download.contains("http://") && !appliance.download.contains("https://")) {
                appliance.download = externalApplianceProperties.path + appliance.download;
            }
            appliances.add(appliance);
        }
        return appliances;
    }
    fun createApplianceRegistryFromFileConfig(filePath: String): ApplianceRegistry {
        var config = loadFromConfig(filePath);
        val appliancesList = getAppliances(config);

        val lastUpdated = ZonedDateTime.now(ZoneId.of("UTC"));

        val registry = ApplianceRegistry(config.registryName,lastUpdated, appliancesList);
        return registry;
    }

    fun writeRegistryToFile(registry: ApplianceRegistry, outputFile: File) {
        mapper.writerWithDefaultPrettyPrinter().writeValue(outputFile, registry);
    }

}

fun main(args: Array<String>) {
    if (args.size < 2) {
        System.err.println("ERROR: Required arguments: <configfile> <outputfile>");
        System.exit(1);
    }
    val creator = CreateFromExternal();
    val registry = creator.createApplianceRegistryFromFileConfig(args[0]);

    val outputFile = File(args[1]);
    creator.writeRegistryToFile(registry, outputFile);
}
