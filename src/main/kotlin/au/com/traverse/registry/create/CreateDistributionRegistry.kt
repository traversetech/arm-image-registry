package au.com.traverse.registry.create;

import org.apache.hc.client5.http.impl.classic.HttpClients;

import au.com.traverse.registry.distributions.*;
import au.com.traverse.registry.model.*;

import kotlinx.serialization.*
import kotlinx.serialization.builtins.*;
import kotlinx.serialization.json.*

import java.io.File;
import java.io.FileNotFoundException

import java.nio.file.Files;
import java.nio.file.StandardOpenOption

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.jsoup.nodes.Document;



const val USER_AGENT = "ArmImageRegistryCreator Bot https://gitlab.com/traversetech/arm-image-registry";

fun loadFromPreviousState(filePath: String) : List<BaseDistribution> {
    val jsonEncoder = Json { prettyPrint = true; ignoreUnknownKeys = true };

    val stateFile = File(filePath);
    println(stateFile.getAbsolutePath());
    val stateBytes : ByteArray = Files.readAllBytes(stateFile.toPath());
    val stateString = String(stateBytes);
    var distributionStates : List<BaseDistribution> = jsonEncoder.decodeFromString(ListSerializer(BaseDistribution.serializer()), stateString);
    return distributionStates;
}

fun writeState(filePath: String, distributions: List<BaseDistribution>) {
    val stateFile = File(filePath);
    
    val jsonEncoder = Json { prettyPrint = true; ignoreUnknownKeys = true };

    val distroState = jsonEncoder.encodeToString(distributions);
    println(distroState);

    Files.write(stateFile.toPath(), distroState.encodeToByteArray(), StandardOpenOption.WRITE);

}

fun main(args: Array<String>) {
    val launchArguments: Array<String>;
    val systemExecProperty = System.getProperty("exec.args");
    if (args.size < 2 && systemExecProperty != null) {
        launchArguments = systemExecProperty.split(" ").toTypedArray();
    } else if (args.size >= 2) {
        launchArguments = args;
    } else {
        System.err.println("ERROR: Two arguments required");
        System.err.println("state.json output.json [hardwareoverride.json]");
        System.exit(1);
        throw IllegalArgumentException("error - failed to exit");
    }

    var compatibilityDatabase : CompatibilityDatabase ?= null;
    if (launchArguments.size >= 3) {
        compatibilityDatabase = loadCompatibilityDatabaseFromFile(launchArguments[2]);
    }

    val httpClient = HttpClients.custom()
                        .setUserAgent(USER_AGENT)
                        .build();

    var distributionStates = loadFromPreviousState(launchArguments[0]);

    var appliances = mutableListOf<Appliance>();
    for(distro in distributionStates) {
        println(distro::class.qualifiedName+":"+distro.hasChanges(httpClient));
        val thisDistroAppliances = distro.getAppliances(httpClient);
        for (appliance in thisDistroAppliances) {
            if (compatibilityDatabase != null) {
                appliance.incompat_hardware = compatibilityDatabase.checkForHardwareIncompatMatch(appliance.id);
                val applinanceProperties = mutableMapOf<String,String>();
                if (appliance.properties != null) {
                    applinanceProperties.putAll(appliance.properties!!);
                }
                applinanceProperties.putAll(compatibilityDatabase.getPropertiesForAppliance(appliance.id));
                appliance.properties = applinanceProperties;
            }
            appliances.add(appliance);
        }
    }

    writeState(launchArguments[0], distributionStates);
    val lastUpdated = ZonedDateTime.now(ZoneId.of("UTC"));

    val registry = ApplianceRegistry("official-distro", lastUpdated, appliances);

    val mapper = ObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    mapper.registerModule(JavaTimeModule());
    
    val outputFile = File(launchArguments[1]);
    //val outputWriter = FileWriter(outputFile);
    mapper.writerWithDefaultPrettyPrinter().writeValue(outputFile, registry);
    //val newRegistryJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(registry);
    //println(newRegistryJson);

}