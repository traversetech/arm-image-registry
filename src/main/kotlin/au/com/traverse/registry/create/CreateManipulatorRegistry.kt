package au.com.traverse.registry.create;

import au.com.traverse.registry.model.Manipulator;

import au.com.traverse.registry.model.ManipulatorRegistry;

import java.io.File;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.apache.commons.codec.digest.DigestUtils;

import org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_256;

import java.time.LocalDateTime;
import java.time.ZoneId;

fun getSHASUM(manipFile: File): String {
    val hexDigest = DigestUtils(SHA_256).digestAsHex(manipFile);
    return hexDigest;
}
fun main(args: Array<String>) {
    if (args.size < 1) {
        println("ERROR: Arguments required");
        System.exit(1);
    }
    val manipulatorFile = File(args[0]);
    if (!manipulatorFile.exists()) {
        println("ERROR: File " + manipulatorFile.getPath() + " does not exist");
        System.exit(1);
    }

    var outputFile : File ?= null;
    if (args.size >= 2) {
        outputFile = File(args[1]);
        val parentDir = outputFile.getParentFile();
        if (!parentDir.exists()) {
            println("${parentDir.getAbsolutePath()} does not exist, creating it");
            parentDir.mkdirs();
        }
    }

    val baseDir = manipulatorFile.getParentFile();

    val mapper = ObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    mapper.registerModule(JavaTimeModule());

    //val manipulatorsJSON = mapper.readTree(manipulatorFile);
    //println(manipulatorsJSON.toString());
    println("Reading manipulators file");
    val localRegistry = mapper.readValue(manipulatorFile, ManipulatorRegistry::class.java);
    var newManipulatorList = mutableListOf<Manipulator>();
    println(localRegistry.toString());
    if (localRegistry.manipulators != null) {
        for(manip in localRegistry.manipulators) {
            println("Manipulator: " + manip.id);
            println("\tDescription: " + manip.description);
            
            val manipFile = File(baseDir, manip.file);
            if (!manipFile.exists()) {
                System.err.println("ERROR: " + manipFile.getPath() + " does not exist in " + baseDir.getPath());
                System.exit(1);
            }
            val manipHex = getSHASUM(manipFile);
            println("\tChecksum: " + manipHex);
            manip.shasum = manipHex;
            newManipulatorList.add(manip);
        }
    } else {
        println("ERROR: No manipulators defined");
        System.exit(1);
    }
    //val localRegistry = ManipulatorRegistry();
    //println(localRegistry.toString());
    val modifiedTime = LocalDateTime.now(ZoneId.of("UTC"));
    val newRegistry = ManipulatorRegistry(localRegistry.registryName, null, modifiedTime, newManipulatorList);
    //println(newRegistry.toString());

    val objectWriter = mapper.writerWithDefaultPrettyPrinter();
    val newRegistryJson = objectWriter.writeValueAsString(newRegistry);
    
    println(newRegistryJson);
    if (outputFile != null) {
        objectWriter.writeValue(outputFile, newRegistry);
    }
}