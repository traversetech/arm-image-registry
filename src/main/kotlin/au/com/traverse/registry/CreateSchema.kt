package au.com.traverse.registry

import com.fasterxml.jackson.databind.JsonNode;
import com.github.victools.jsonschema.generator.OptionPreset;
import com.github.victools.jsonschema.generator.SchemaGenerator;
import com.github.victools.jsonschema.generator.SchemaGeneratorConfig;
import com.github.victools.jsonschema.generator.SchemaGeneratorConfigBuilder;
import com.github.victools.jsonschema.generator.SchemaVersion;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import au.com.traverse.registry.model.*;

import java.io.File;

fun writeFile(dataNode: ObjectNode, outputFile: File) {
    val mapper = ObjectMapper();
    val writer = mapper.writerWithDefaultPrettyPrinter();
    writer.writeValue(outputFile, dataNode);
}
fun main(args: Array<String>) {
    val configBuilder = SchemaGeneratorConfigBuilder(SchemaVersion.DRAFT_2019_09, OptionPreset.PLAIN_JSON);
    val config = configBuilder.build();
    val generator = SchemaGenerator(config);
    var schemaOutputDir : File ?= null;
    if (args.size >= 1) {
        schemaOutputDir = File(args[0]);
        schemaOutputDir.mkdirs();
        if (!schemaOutputDir.exists()) {
            System.err.println("ERROR: ${schemaOutputDir.absolutePath} does not exist");
            System.exit(1);
        } else if (!schemaOutputDir.isDirectory) {
            System.err.println("ERROR: ${schemaOutputDir.absolutePath} is not a directory");
            System.exit(1);
        }
    }
    println("** Appliance Schema**");
    val applianceSchema = generator.generateSchema(Appliance::class.java);
    println(applianceSchema.toString());
    if (schemaOutputDir != null) {
        val applianceSchemaFile = File(schemaOutputDir, "appliance.json");
        writeFile(applianceSchema, applianceSchemaFile);
    }

    println("** Appliance registry Schema**");
    val applianceRegistrySchema = generator.generateSchema(ApplianceRegistry::class.java);
    println(applianceRegistrySchema.toString());
    if (schemaOutputDir != null) {
        val registrySchemaFile = File(schemaOutputDir, "appliance-registry.json");
        writeFile(applianceRegistrySchema, registrySchemaFile);
    }

    println("** Manipulator Schema**");
    val manipulatorSchema = generator.generateSchema(Manipulator::class.java);
    println(manipulatorSchema.toString());
    
    if (schemaOutputDir != null) {
        val manipulatorSchemaFile = File(schemaOutputDir, "manipulator-schema.json");
        writeFile(manipulatorSchema, manipulatorSchemaFile);
    }

    println("** Manipulator Registry Schema**");
    val manipulatorRegistrySchema = generator.generateSchema(ManipulatorRegistry::class.java);
    println(manipulatorRegistrySchema.toString());
    
    if (schemaOutputDir != null) {
        val manipulatorRegistrySchemaFile = File(schemaOutputDir, "manipulator-registry.json");
        writeFile(manipulatorRegistrySchema, manipulatorRegistrySchemaFile);
    }
}