package au.com.traverse.registry.distributions;

import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.ClassicHttpResponse;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import au.com.traverse.registry.model.Appliance;
import kotlinx.serialization.*;
import kotlinx.serialization.json.*;
import kotlin.text.String;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@kotlinx.serialization.Serializable
class UbuntuDistributionState {
    var etag: String = ""
}

@kotlinx.serialization.Serializable
class UbuntuReleaseProperties() {
};

@kotlinx.serialization.Serializable
class UbuntuDistribution (val ubuntuBaseURL: String,
    val releasesToPublish: Map<String,UbuntuReleaseProperties>): BaseDistribution() {

    var state: UbuntuDistributionState = UbuntuDistributionState()
    override fun hasChanges(http: HttpClient): Boolean {
        return true;
    }
    fun getLatestImageDetails(versionsTree: ObjectNode) : ObjectNode? {
        val ddmmyylength = "20200512".count()
        var latestImageDate = LocalDate.MIN
        var latestImageTree : ObjectNode  ?= null
        for(version in versionsTree.fieldNames()) {
            // Some versions were revised, e.g 20200512.1
            var versionddmmyy : String
            if (version.count() >= ddmmyylength)
                versionddmmyy = version.substring(0,ddmmyylength)
            else 
                versionddmmyy = version;

            var thisVersionDate = LocalDate.parse(versionddmmyy, DateTimeFormatter.BASIC_ISO_DATE);
            if (thisVersionDate.isAfter(latestImageDate)) {
                latestImageTree = versionsTree.get(version) as ObjectNode;
                latestImageDate = thisVersionDate;
            }
        }
        return latestImageTree;
    }
    fun createApplianceFromJSON(productTree: ObjectNode) : Appliance {
        var applianceId = "ubuntu-" + productTree.get("release").asText();
        var releaseCodeName = productTree.get("release_codename").asText();
        var releaseTitle = productTree.get("release_title").asText();

        var applianceName = "Ubuntu Server " + releaseTitle + " (\""+releaseCodeName +"\")";
        var applianceDescription =  "Ubuntu " + releaseTitle + " server image";

        var versionTree = productTree.get("versions") as ObjectNode;
        var latestImage = getLatestImageDetails(versionTree);
        if (latestImage == null) {
            println("ERROR: No image found");
            throw IllegalArgumentException("No image found");
        }
        
        var imageItems = latestImage.get("items") as ObjectNode;
        var disk1img = imageItems.get("disk1.img") as ObjectNode;

        println(disk1img.toString());

        var blankAppliance = Appliance(id = applianceId,
            name = applianceName,
            description = applianceDescription,
            maintainer = "Ubuntu",
            version = "20210511",
            download = "https://cloud-images.ubuntu.com/" + disk1img.get("path").asText(),
            link = "https://cloud-images.ubuntu.com/",
            checksum = "sha256:"+disk1img.get("sha256").asText(),
            mindisk = 4096,
            minram = 1024,
            supported_hardware = null,
            incompat_hardware = null,
            cloudinit = true,
            );
        return blankAppliance;
    }
    fun getUbuntuList(httpClient: HttpClient) : List<Appliance> {
        val applianceList = ArrayList<Appliance>();
        val get = HttpGet(ubuntuBaseURL);
        var mapper = ObjectMapper();
        try {
            var response = httpClient.execute(get) as ClassicHttpResponse;
            var etag = response.getHeader("ETag");
            println("ETAG: " + etag.getValue());
            state.etag = etag.getValue().replace("\"","");
            println(response.getCode());
            var entity = response.getEntity();
            var entityTree = mapper.readTree(entity.getContent()) as ObjectNode;
            var products = entityTree.get("products");
            for(product in releasesToPublish.keys) {
                var thisProduct = products.get(product) as ObjectNode;

                var thisReleaseAppliance = createApplianceFromJSON(thisProduct);
                applianceList.add(thisReleaseAppliance);
            }

            EntityUtils.consume(entity);
        } catch (e: IOException) {
            e.printStackTrace(System.err);
        }
        return applianceList;
    }
    override fun getAppliances(http: HttpClient): List<Appliance> {
        println("Getting Ubuntu from " + ubuntuBaseURL);
        val appliances = getUbuntuList(http);
        
        return appliances;
    }
}