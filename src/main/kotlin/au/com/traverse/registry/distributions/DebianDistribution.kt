package au.com.traverse.registry.distributions;

import au.com.traverse.registry.model.Appliance;
import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.jsoup.Jsoup;
import java.io.InputStream;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.jsoup.nodes.Document;
import java.time.LocalDate;
import java.util.regex.Pattern;
import java.time.format.DateTimeFormatter;

@kotlinx.serialization.Serializable
class DebianReleaseProperties(
    val url : String,
    val codename: String,
    val versionNum: Int ?= null,
    val currentRelease: Boolean) {
};

@kotlinx.serialization.Serializable
class DebianDistribution (override val fileNameSelector: String, val releasesToPublish: Map<String,DebianReleaseProperties>): IndexScraperDistribution() {
    fun getLatestBuildURL(http: HttpClient, parentURL: String): String? {
        val buildIndex = getIndexFolderDocument(http, parentURL);
        var latestBuildDate = LocalDate.MIN;
        var latestBuildURL : String? = null;
        val links = buildIndex.select(fileNameSelector);
        val buildNamePattern = Pattern.compile("([\\d]{8})-[\\d]+/");
        for (link in links) {
            val thisLinkText = link.text();
            val matcher = buildNamePattern.matcher(thisLinkText)
            if (!matcher.matches())
                continue;
            val yyyymmddgroup = matcher.group(1);
            val thisBuildDate = LocalDate.parse(yyyymmddgroup, DateTimeFormatter.BASIC_ISO_DATE);
            if (thisBuildDate.isAfter(latestBuildDate)) {
                latestBuildDate = thisBuildDate;
                latestBuildURL = link.attr("abs:href");
            }
        }
        return latestBuildURL;
    }
    override fun getAppliances(http: HttpClient): List<Appliance> {
        val applianceList = mutableListOf<Appliance>();
        for (version in releasesToPublish.keys) {
            val thisReleaseProperties = releasesToPublish.get(version);
            if (thisReleaseProperties == null) {
                System.err.println("ERROR: No release properties for " + version);
                continue;
            }
            val versionString: String
            if (thisReleaseProperties.versionNum != null)
                versionString = "${thisReleaseProperties.versionNum}"
            else
                versionString = thisReleaseProperties.codename;
            val dailyFileAppend : String;
            if (!thisReleaseProperties.currentRelease) {
                dailyFileAppend = "-daily";
            } else {
                dailyFileAppend = "";
            }
            val imageNamePattern = "debian-${versionString}-generic-arm64${dailyFileAppend}-([\\d]{8}-[\\d]+).qcow2"
            println(imageNamePattern);

            val getURL : String
            if (thisReleaseProperties.currentRelease)
                getURL = thisReleaseProperties.url;
            else
                getURL = thisReleaseProperties.url + "daily/";
            
            val latestBuildURL = getLatestBuildURL(http, getURL);
            if (latestBuildURL == null) {
                System.err.println("ERROR: Could not determine latest build for " + version);
                continue;
            }

            val matchingFileElement = findMatchingLinkForFilename(http, latestBuildURL, imageNamePattern, false);
            if (matchingFileElement == null) {
                System.err.println("Unable to find matching file for " + version);
                continue;
            }
            val matchingFileLink = matchingFileElement.attr("abs:href");
            var matchingFileName = matchingFileElement.text();

            val imageDatePattern = Pattern.compile(imageNamePattern);
            val imageDateMatch = imageDatePattern.matcher(matchingFileName);
            if (!imageDateMatch.matches()) {
                System.err.println("ERROR: " + matchingFileName + " does not match " + imageNamePattern);
                continue;
            }
            val imageDateGroup = imageDateMatch.group(1);
            
            val imageChecksum = getChecksum(http, latestBuildURL+"SHA512SUMS",matchingFileName);
            if (imageChecksum == null) {
                System.err.println("ERROR: Could not find checksum for file ${matchingFileName}");
                continue;
            }

            val applianceName = "Debian ${version} (${versionString})"
            val applianceDescription = "Debian ${version} (codename ${thisReleaseProperties.codename})";

            val newAppliance = Appliance(id = "debian-"+version,
            name = applianceName,
            description = applianceDescription,
            maintainer = "Debian",
            version = imageDateGroup,
            download = matchingFileLink,
            link = "https://cloud.debian.org/images/cloud/buster/",
            checksum = "sha512:${imageChecksum.checksum}",
            mindisk = 4096,
            minram = 1024,
            supported_hardware = null,
            incompat_hardware = null,
            cloudinit = true,
            );
            applianceList.add(newAppliance);
        }
        return applianceList;
    }
    override fun hasChanges(http: HttpClient): Boolean {
        return true;
    }
}