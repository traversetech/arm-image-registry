package au.com.traverse.registry.distributions;

import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.core5.http.io.entity.EntityUtils;

import org.jsoup.Jsoup;
import java.io.InputStream;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.regex.Pattern;

class ChecksumPair(
    val checksum: String,
    val filename: String
) {

};


@kotlinx.serialization.Serializable
sealed class IndexScraperDistribution (): BaseDistribution() {
    abstract val fileNameSelector: String
    fun getIndexFolderDocument(http: HttpClient, folderURL: String) : Document {
        val thisFolderGet = HttpGet(folderURL);
        val thisReleaseFolderResponse = http.execute(thisFolderGet) as ClassicHttpResponse;
        val thisReleaseEntity = thisReleaseFolderResponse.getEntity();
        val thisReleaseFolderHTMLStream : InputStream = thisReleaseEntity.getContent();
        val thisReleaseFolderHTMLDocument = Jsoup.parse(thisReleaseFolderHTMLStream, null, folderURL);
        
        EntityUtils.consumeQuietly(thisReleaseEntity);
        thisReleaseFolderResponse.close();
        return thisReleaseFolderHTMLDocument;
    }
    fun getChecksum(http: HttpClient, checksumURL: String, matchFileName: String?): ChecksumPair? {
        val checksumResponse = http.execute(HttpGet(checksumURL)) as ClassicHttpResponse;
        val checksumEntity = checksumResponse.getEntity();
        val checksumsString = EntityUtils.toString(checksumEntity);
        EntityUtils.consumeQuietly(checksumEntity);
        
        val checksumLines = checksumsString.split("\n");
        for(line in checksumLines) {
            val filename : String;
            val checksum : String;
            // OpenSSL
            if (line.contains("=")) {
                var split  = line.split(" = ");
                var bracketStart = split[0].indexOf("(");
                var bracketEnd = split[0].indexOf(")");
                filename = split[0].substring(bracketStart+1,bracketEnd);
                checksum = split[1].trim();
            } else {
                val split = line.split(" ");
                checksum = split[0].trim();
                filename = split.last().trim();
            }
            if (matchFileName == null || matchFileName.equals(filename))
                return ChecksumPair(checksum,filename);
            
        }
        return null;
    }

    fun findMatchingLinksForExpression(http: HttpClient, folderURL: String, pattern: Pattern) : List<Element> {
        val matchingElements = mutableListOf<Element>();
        val indexFolderDocument = getIndexFolderDocument(http, folderURL);
        val fileLinks = indexFolderDocument.select(fileNameSelector);
    
        for(fileLinkElement in fileLinks) {
            val thisLinkFileName = fileLinkElement.text();
            val matcher = pattern.matcher(thisLinkFileName);
            if (matcher.matches())
                matchingElements.add(fileLinkElement);
        }
        return matchingElements;
    }
    
    fun findMatchingLinkForFilename(http: HttpClient, folderURL: String, fileNameString: String, exactMatch: Boolean) : Element? {
        val indexFolderDocument = getIndexFolderDocument(http, folderURL);
        val fileLinks = indexFolderDocument.select(fileNameSelector);
        var pattern : Pattern ?= null;
    
        if (!exactMatch) {
            pattern = Pattern.compile(fileNameString);
        }
        for(fileLinkElement in fileLinks) {
            val thisLinkFileName = fileLinkElement.text();
            if (exactMatch && thisLinkFileName.equals(fileNameString)) {
                return fileLinkElement;
            } else if (!exactMatch && pattern != null) {
                val matcher = pattern.matcher(thisLinkFileName);
                if (matcher.matches())
                    return fileLinkElement;
            }
        }
        return null;
    }
}