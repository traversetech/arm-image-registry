package au.com.traverse.registry.distributions;

import au.com.traverse.registry.model.Appliance;
import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.jsoup.Jsoup;
import java.io.InputStream;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.jsoup.nodes.Document;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.IOException;

@kotlinx.serialization.Serializable
class OpenSUSEReleaseProperties(
    val url : String,
    val basename: String,
    val format: String,
    val friendlyName: String,
    val moreInfoURL: String,
    val flavourName: String,
    var cloudinit: Boolean = false,
    var mindisk: Int = 4096,
    var versionVariant: String? = null) {
};

class SHA256SUMPair(
    val sha256sum: String,
    val filename: String
) {

};

@kotlinx.serialization.Serializable
class OpenSUSEDistribution (override val fileNameSelector: String, val releasesToPublish: Map<String,OpenSUSEReleaseProperties>): IndexScraperDistribution() {
    /**
     * Download the JSON format folder index
     * @param http The HTTP Client instance to use
     * @param thisFolderURL The folder URL
     */
    private fun getOpenSuseFolderIndex(http: HttpClient, thisFolderURL: String): ArrayNode {
        val thisJSONIndexURL = thisFolderURL + "?jsontable";
        println("Getting JSON Index: ${thisJSONIndexURL}");
        val thisJSONGetRequest = HttpGet(thisJSONIndexURL);
       
        var mapper = ObjectMapper();
        val thisJSONGetResponse = http.execute(thisJSONGetRequest) as ClassicHttpResponse;
        val contentType = thisJSONGetResponse.getHeader("Content-Type");
        if (contentType == null || !contentType.getValue().startsWith("application/json")) {
            println("ERROR: Response is not JSON: ${contentType}");
            throw IOException("Unexpected content type ${contentType}");
        }
        val entity = thisJSONGetResponse.getEntity();
            
        val entityTree = mapper.readTree(entity.getContent()) as ObjectNode;
        EntityUtils.consume(entity);

        val dataTree = entityTree.get("data");
        if (!dataTree.isArray) {
            println("ERROR: \"data\" node is not an array");
            throw IOException("Unexpected node type");
        }
        return dataTree as ArrayNode;
    }
    /**
     * Compare the "mtime" and "size" attributes of the file JSON information node
     * @return true if the file details are the same
     */
    private fun compareFileDataSizeAndTime(exactNameData: ObjectNode, exactBuildData: ObjectNode): Boolean {
        val exactNameSize = exactNameData.get("size");
        val exactBuildSize = exactBuildData.get("size");
        if (exactNameSize != exactBuildSize) {
            println("Sizes do not match: ${exactNameSize} ${exactBuildSize}");
            return false;
        }
        val exactNameMtime = exactNameData.get("mtime");
        val exactBuildMtime = exactBuildData.get("mtime");
        if (exactNameMtime != exactBuildMtime)
            return false;
        return true;
    }
    private fun findMatchingFilename(http: HttpClient, folderURL: String, imageFileNameString: String, versionVariant: String?): String? {
        val folderIndex = getOpenSuseFolderIndex(http, folderURL);
        val imageArchExtensionLocation = imageFileNameString.indexOf(".aarch64");
        // The basename is up to the "." after aarch64
        val imageBaseName = imageFileNameString.substring(0, imageArchExtensionLocation+8);
        val imageExtension = imageFileNameString.substring(imageFileNameString.lastIndexOf("."));
        println("Image base name: ${imageBaseName} extension: ${imageExtension}");
        var exactFileMatchData: ObjectNode? = null;
        var exactBuildMatchData: ObjectNode? = null;
        
        for(thisFileData in folderIndex.elements()) {
            val thisFileNameNode = thisFileData.get("name");
            if (thisFileNameNode == null)
                continue;
            val thisFileName = thisFileNameNode.textValue();
            val fileNameBuildLocation = thisFileName.indexOf("Build");
            val fileNameSnapshotLocation = thisFileName.indexOf("Snapshot");
            var indexOfBuildOrSnapshot = fileNameBuildLocation;
            if (fileNameBuildLocation < 0 && fileNameSnapshotLocation > 0)
                indexOfBuildOrSnapshot = fileNameSnapshotLocation;

            if (thisFileName == imageFileNameString) {
                exactFileMatchData = thisFileData as ObjectNode;
            } else if (thisFileName.startsWith(imageBaseName) && 
                (indexOfBuildOrSnapshot > 0) && thisFileName.endsWith(imageExtension)) {
                if (versionVariant != null) {
                    val indexOfAfterVersion = thisFileName.indexOf("-", imageBaseName.length + 1);
                    val imageVariantName = thisFileName.substring(indexOfAfterVersion + 1, indexOfBuildOrSnapshot-1);
                    if (!versionVariant.equals(imageVariantName))
                        continue;
                }
                println("Exact build/snapshot filename candidate: ${thisFileName}");
                exactBuildMatchData = thisFileData as ObjectNode;
            }
        }
        if (exactFileMatchData != null && exactBuildMatchData != null) {
            if (compareFileDataSizeAndTime(exactFileMatchData, exactBuildMatchData)) {
                val exactBuildFileName = exactBuildMatchData.get("name").textValue();
                return folderURL + exactBuildFileName;
            } else {
                println("WARNING: Size or mtime does not match")
            }
        }
        return null;
    } 

    override fun getAppliances(http: HttpClient): List<Appliance> {
        val applianceList = mutableListOf<Appliance>();
        for (releaseId in releasesToPublish.keys) {
            val thisReleaseProperties = releasesToPublish.get(releaseId);
            if (thisReleaseProperties == null) {
                System.err.println("WARNING: " + releaseId + " has no data");
                continue;
            }
            val thisReleaseURL = thisReleaseProperties.url;
            println(releaseId + " at " + thisReleaseURL);

            val thisReleaseImageURL = findMatchingFilename(http, thisReleaseURL, thisReleaseProperties.basename, thisReleaseProperties.versionVariant);
            if (thisReleaseImageURL == null) {
                println("WARNING: Unable to find download for ${releaseId}");
                continue;
            }
            var thisReleaseSHALink = thisReleaseImageURL+".sha256";

            println("\tImage: " + thisReleaseImageURL);
            println("\tSHA256 file: " + thisReleaseSHALink); 

            val applianceName = "OpenSUSE ${thisReleaseProperties.flavourName} ${thisReleaseProperties.friendlyName}";
            val descriptionText = when (thisReleaseProperties.flavourName) {
                "JeOS" -> "(\"Just Enough Operating System\") Root password is \"linux\""
                "MicroOS" -> "(\"Micro Service OS\")"
                else -> ""
            };
            val applianceDescription = "OpenSuSE ${thisReleaseProperties.flavourName} ${descriptionText}";

            val sha256data = getChecksum(http, thisReleaseSHALink, null);
            if (sha256data == null) {
                System.err.println("ERROR retreiving sha256sum for ${releaseId}");
                continue;
            }
            println(sha256data.checksum);
            println(sha256data.filename); 
            /* the SHA256SUM file contains the original build filename, use that to
             * determine the build date/version
             */
            val fileNameDatePart = sha256data.filename.split(".aarch64-").last()
            val fileExtension = fileNameDatePart.lastIndexOf('.');
            val buildDateVersion = fileNameDatePart.substring(0, fileExtension);
            println(buildDateVersion);

            val imageFormat = thisReleaseProperties.format;

            val newAppliance = Appliance(id = releaseId,
                name = applianceName,
                description = applianceDescription,
                maintainer = "OpenSUSE",
                version = buildDateVersion,
                download = thisReleaseImageURL,
                link = thisReleaseProperties.moreInfoURL,
                checksum = "sha256:${sha256data.checksum}",
                mindisk = thisReleaseProperties.mindisk,
                minram = 1024,
                format = imageFormat,
                supported_hardware = null,
                incompat_hardware = null,
                cloudinit = thisReleaseProperties.cloudinit
            );
            applianceList.add(newAppliance);
        } 
        return applianceList;
    }
    override fun hasChanges(http: HttpClient): Boolean {
        return true;
    }
}