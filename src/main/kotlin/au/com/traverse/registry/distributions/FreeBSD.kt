package au.com.traverse.registry.distributions;

import au.com.traverse.registry.model.Appliance;
import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.jsoup.Jsoup;
import java.io.InputStream;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.jsoup.nodes.Document;

@kotlinx.serialization.Serializable
class FreeBSDReleaseProperties(
    val url : String,
    val filename: String)
    {
};

@kotlinx.serialization.Serializable
class FreeBSD (override val fileNameSelector: String, val releasesToPublish: Map<String,FreeBSDReleaseProperties>): IndexScraperDistribution() {
    override fun getAppliances(http: HttpClient): List<Appliance> {
        var appliances = mutableListOf<Appliance>();
        for ((version, releaseProperties) in releasesToPublish) {
            var thisReleaseLink = findMatchingLinkForFilename(http, releaseProperties.url, releaseProperties.filename, true);
            if (thisReleaseLink == null) {
                System.err.println("Could not get URL for FreeBSD ${version}");
                continue;
            }
            var thisReleaseDownload = thisReleaseLink.attr("abs:href");
            var checksumPair = getChecksum(http, "${releaseProperties.url}CHECKSUM.SHA256", releaseProperties.filename);
            if (checksumPair == null) {
                System.err.println("Could not get SHA256SUM for ${releaseProperties.filename}");
                continue
            }
            val thisReleaseAppliance = Appliance(
                id = "freebsd-${version}",
                name = "FreeBSD ${version}",
                description = "FreeBSD ${version}",
                version = version,
                maintainer = "freebsd.org",
                download = thisReleaseDownload,
                link = "https://www.freebsd.org/where/",
                checksum = "sha256:${checksumPair.checksum}",
                format = "raw-xz",
                mindisk = 6144,
                minram = 1024,
                supported_hardware = null,
                incompat_hardware = null,
                cloudinit = false
            );
            appliances.add(thisReleaseAppliance);
        }
        return appliances;
    }
    override fun hasChanges(http: HttpClient): Boolean {
        return true;
    }
}