package au.com.traverse.registry.distributions;

import au.com.traverse.registry.model.Appliance;
import au.com.traverse.registry.distributions.BaseDistribution

import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.core5.http.io.entity.EntityUtils;

import java.lang.ProcessBuilder;
import java.lang.Process;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.MissingNode;

import java.io.IOException;

@kotlinx.serialization.Serializable
class RockstorReleaseProperties (
    val url: String,
    val checksum: String,
    val version: String,
    val link: String) {
};

@kotlinx.serialization.Serializable
class RockstorDistribution (val releasesToPublish: Map<String,RockstorReleaseProperties>): BaseDistribution() {

    fun getSHA256SUM(http: HttpClient, sha256SUMHref: String): SHA256SUMPair {
        val sha256SUMResponse = http.execute(HttpGet(sha256SUMHref)) as ClassicHttpResponse;
        val sha256SUMEntity = sha256SUMResponse.getEntity();
        val sha256SUMString = EntityUtils.toString(sha256SUMEntity);
        EntityUtils.consumeQuietly(sha256SUMEntity);
        
        val split = sha256SUMString.split(" ");
        val sha256sum = split[0].trim();
        val filename = split.last().trim();

        return SHA256SUMPair(sha256sum,filename);
    }

    override fun getAppliances(http: HttpClient): List<Appliance> {
        val appliancesToPublish = mutableListOf<Appliance>();
        for (release in releasesToPublish.keys) {
            println("Release to publish: ${release}");
            val releaseInfo = releasesToPublish[release];
            if (releaseInfo == null) {
                System.err.println("ERROR: Release ${release} has no data");
                continue;
            }
            val sha256sum = getSHA256SUM(http, releaseInfo.checksum);
            val customProperties = mapOf("x-muvirt-require-disk-serial" to "true");
            val newAppliance = Appliance(id = release,
                name = "Rockstor ${release}",
                description = "Rockstor NAS",
                maintainer = "Rockstor",
                version = releaseInfo.version,
                download = releaseInfo.url,
                link = releaseInfo.link,
                checksum = "sha256:${sha256sum.sha256sum}",
                mindisk = 16384,
                minram = 2048,
                format = "qcow2",
                supported_hardware = null,
                incompat_hardware = null,
                cloudinit = false,
                properties = customProperties
            );
            appliancesToPublish.add(newAppliance);
        }
        return appliancesToPublish;
    }
    override fun hasChanges(http: HttpClient): Boolean {
        return true;
    }
}