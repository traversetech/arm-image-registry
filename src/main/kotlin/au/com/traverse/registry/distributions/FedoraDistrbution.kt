package au.com.traverse.registry.distributions;

import au.com.traverse.registry.model.Appliance;
import au.com.traverse.registry.distributions.BaseDistribution

import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.core5.http.io.entity.EntityUtils;

import java.lang.ProcessBuilder;
import java.lang.Process;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.MissingNode;

import java.io.IOException;
/* @kotlinx.serialization.Serializable
class FedoraReleaseProperties(
    val release: Int
) */

@kotlinx.serialization.Serializable
class FedoraDistribution (val releasesToPublish: List<Int>): BaseDistribution() {
    fun callGetFedoraCommand(releaseNum: Int): ObjectNode {
        val possiblePythons = listOf("python","python3");
        for (pythonExec in possiblePythons) {
            try {
                val processBuilder = ProcessBuilder(pythonExec,"src/main/python/getfedora.py",releaseNum.toString());
                val process = processBuilder.start();
                val processInputStream = process.getInputStream();
                val allProcessOutputBytes = processInputStream.readBytes();
                process.waitFor();

                val objectMapper = ObjectMapper();
                val fedoraReleaseTree = objectMapper.readTree(allProcessOutputBytes);
                val childProcessExitValue = process.exitValue();
                if (childProcessExitValue != 0) {
                    println("Non-zero exit value: ${process.exitValue()}");
                    val processOutputString = String(allProcessOutputBytes);
                    println("Fedfind helper output was: ${processOutputString}");
                    continue;
                }
                if (fedoraReleaseTree is MissingNode)
                    continue;

                println(fedoraReleaseTree.toString());
                return fedoraReleaseTree as ObjectNode;
            } catch (e: IOException) {
                println("Could not execute ${pythonExec} as the python interpreter");
                println("(Trying the next possibility)");
            }
        }
       throw Exception("Could not execute the fedora python script");
    }

    override fun getAppliances(http: HttpClient): List<Appliance> {
        val appliances = mutableListOf<Appliance>();
        for(releaseNum in releasesToPublish) {
            val thisReleaseData = callGetFedoraCommand(releaseNum);
            val thisImageDirectURL = thisReleaseData.get("direct_url").asText();
            val thisImageChecksum = thisReleaseData.get("checksums").get("sha256").asText();
            val thisImageComposeId = thisReleaseData.get("compose_id").asText();

            val thisApplianceId = "fedora-${releaseNum}";
            val thisApplianceName = "Fedora ${releaseNum} Server/Cloud"

            val newAppliance = Appliance(id = thisApplianceId,
                name = thisApplianceName,
                description = thisApplianceName,
                maintainer = "Fedora",
                version = thisImageComposeId,
                download = thisImageDirectURL,
                link = "https://alt.fedoraproject.org/cloud/",
                checksum = "sha256:${thisImageChecksum}",
                mindisk = 6144,
                minram = 1024,
                supported_hardware = null,
                incompat_hardware = null,
                cloudinit = true
            );
            appliances.add(newAppliance);
        }
        return appliances;
    }
    override fun hasChanges(http: HttpClient): Boolean {
        return true;
    }
}