package au.com.traverse.registry.distributions;

import au.com.traverse.registry.model.Appliance;
import org.apache.hc.client5.http.classic.HttpClient;


@kotlinx.serialization.Serializable
sealed class BaseDistribution {
    abstract fun getAppliances(http: HttpClient): List<Appliance>
    abstract fun hasChanges(http: HttpClient): Boolean
}