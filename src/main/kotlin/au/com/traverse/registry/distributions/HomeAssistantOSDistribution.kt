package au.com.traverse.registry.distributions;

import au.com.traverse.registry.model.Appliance;
import au.com.traverse.registry.distributions.BaseDistribution
import au.com.traverse.registry.create.getSHASUM

import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.core5.http.io.entity.EntityUtils;

import java.lang.ProcessBuilder;
import java.lang.Process;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.MissingNode;

import java.io.IOException;

import java.util.regex.Pattern;

import org.apache.commons.codec.digest.DigestUtils;

data class HomeAssistantReleaseProperties (
	val name: String,
	val releaseInfoURL: String,
	val releaseVersion: String,
	val filename: String,
	val releaseDownloadURL: String
);

@kotlinx.serialization.Serializable
class HomeAssistantOSDistribution (val imageNamePattern: String): BaseDistribution() {
	var lastReleaseURL: String ?= null;
	var lastReleaseSHA256: String ?= null;

	fun determineSHA256(http: HttpClient, fileURL: String): String {
		val fileDownloadResponse = http.execute(HttpGet(fileURL)) as ClassicHttpResponse;
		val imageFileEntity = fileDownloadResponse.getEntity();
		val imageFileBytes = EntityUtils.toByteArray(imageFileEntity);
		EntityUtils.consumeQuietly(imageFileEntity);
		val sha256 = DigestUtils.sha256Hex(imageFileBytes);
		return sha256;
	}
	fun getLatestReleaseURL(http: HttpClient): HomeAssistantReleaseProperties? {
		val githubdataurl = "https://api.github.com/repos/home-assistant/operating-system/releases/latest";
		val githubResponse = http.execute(HttpGet(githubdataurl)) as ClassicHttpResponse;
		var entity = githubResponse.getEntity();
		val mapper = ObjectMapper();
		var entityTree = mapper.readTree(entity.getContent()) as ObjectNode;
		if (!entityTree.has("name") || !entityTree.has("assets") || !entityTree.has("url")) {
			System.err.println("ERROR: Releases list does not have \"name\", \"assets\" or \"url\"");
			return null;
		}
		var buildAssets = entityTree.get("assets");
		val buildNamePattern = Pattern.compile(imageNamePattern);
		val releaseName = entityTree.get("name");
		val releaseInfoURL = entityTree.get("url").textValue();
		for(asset in buildAssets) {
			if (!asset.has("name"))
				continue;

			val thisAssetName = asset.get("name").textValue();
			val matcher = buildNamePattern.matcher(thisAssetName);
			if (!matcher.matches())
				continue;
			val releaseVersion = matcher.group(1);
			val downloadURL = asset.get("browser_download_url").textValue();
			println("Filename: ${thisAssetName} Download URL: ${downloadURL}");
			return HomeAssistantReleaseProperties(releaseName.textValue(), 
				releaseInfoURL,
				releaseVersion,
				thisAssetName, 
				downloadURL);
		}
		return null;
	}

	override fun getAppliances(http: HttpClient): List<Appliance> {
		val appliancesToPublish = mutableListOf<Appliance>()
		val latestRelease = getLatestReleaseURL(http)
		if (latestRelease == null) {
			System.err.println("ERROR: No Home Assistant OS releases found")
			return appliancesToPublish
		}
		val releaseSHA256 : String;
		val cachedSHA256 = lastReleaseSHA256;
		if (lastReleaseURL == latestRelease.releaseInfoURL && cachedSHA256 != null)
			releaseSHA256 = cachedSHA256
		else
			releaseSHA256 = determineSHA256(http, latestRelease.releaseDownloadURL)
			val newAppliance =
			Appliance(id = "home-assistant-os",
						  name = "Home Assistant Operating System",
						  description = "${latestRelease.name}",
						  maintainer = "Home Assistant",
						  version = latestRelease.releaseVersion,
						  download = latestRelease.releaseDownloadURL,
						  link = latestRelease.releaseInfoURL,
						  checksum = "sha256:${releaseSHA256}",
						  mindisk = 16384,
						  minram = 2048,
						  format = "raw-xz",
						  supported_hardware = null,
						  incompat_hardware = listOf<String>("traverse,ten64"),
						  cloudinit = false
					);
		appliancesToPublish.add(newAppliance);
		lastReleaseURL = latestRelease.releaseInfoURL;
		lastReleaseSHA256 = releaseSHA256;
			return appliancesToPublish;
	}

	override fun hasChanges(http: HttpClient): Boolean {
		val latestRelease = getLatestReleaseURL(http)
		if (latestRelease == null)
			return true;
		if (latestRelease.releaseInfoURL == lastReleaseURL)
			return false;
		return true;
	}
}