package au.com.traverse.registry.distributions;

import au.com.traverse.registry.model.Appliance;
import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.jsoup.Jsoup;
import java.io.InputStream;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.jsoup.nodes.Document;

import java.time.LocalTime;
import java.util.regex.Pattern;

import org.jsoup.nodes.Element;

@kotlinx.serialization.Serializable
class CentOSReleaseProperties(
    val url : String,
    val regex: String,
    val buildNumGroup: Int,
    val friendlyName: String,
    val moreInfoURL: String) {
};

@kotlinx.serialization.Serializable
class CentOSState(
    val knownHashes: Map<String,String> ?= mapOf<String,String>()
)

@kotlinx.serialization.Serializable
class CentOSDistribution (override val fileNameSelector: String, val releasesToPublish: Map<String,CentOSReleaseProperties>): IndexScraperDistribution() {
    var state: CentOSState = CentOSState();

    override fun getAppliances(http: HttpClient): List<Appliance> {
        var appliances = mutableListOf<Appliance>();
        for ((releaseId, releaseProperties) in releasesToPublish) {
            val centOSFilePattern = Pattern.compile(releaseProperties.regex);

            val matchingFileElements = findMatchingLinksForExpression(http, releaseProperties.url,centOSFilePattern);
            /* As there are multiple versions listed on the same page, iterate through to find the latest */
            var latestBuildNumber = 0;
            var latestBuildElement : Element ?= null;
            for (matchingElement in matchingFileElements) {
                var fileName = matchingElement.text();
                var matchingGroup = centOSFilePattern.matcher(fileName);
                println("File name: ${fileName} Group count: ${matchingGroup.groupCount()}")
                matchingGroup.find();
                
                val buildNumber = matchingGroup.group(releaseProperties.buildNumGroup).toInt();
                println(fileName);
                println("\tBuild: "+buildNumber);
                if (buildNumber > latestBuildNumber) {
                    latestBuildNumber = buildNumber;
                    latestBuildElement = matchingElement;
                }
            }
            if (latestBuildNumber > 0 && latestBuildElement != null) {
                val downloadLink = latestBuildElement.attr("abs:href");
                val filename = latestBuildElement.text()
                var checksum = getChecksum(http, releaseProperties.url+"CHECKSUM", filename);
                var sha256sum : String?;
                /* CentOS sometimes fails to update their CHECKSUM file when a build is released */
                if (checksum == null) {
                    if (state.knownHashes != null && state.knownHashes!!.contains(filename)) {
                        sha256sum = state.knownHashes!!.get(filename);
                    } else {
                        println("ERROR: No checksum found for ${filename}");
                        continue;
                    }
                } else {
                    sha256sum = checksum.checksum;
                }
                val newAppliance = Appliance(id = releaseId,
                name = releaseProperties.friendlyName,
                description = releaseProperties.friendlyName,
                maintainer = "centos.org",
                version = "${latestBuildNumber}",
                download = downloadLink,
                link = releaseProperties.moreInfoURL,
                checksum = "sha256:${sha256sum}",
                mindisk = 10240, /* CentOS is much heavier than other distros */
                minram = 4096,
                supported_hardware = null,
                incompat_hardware = null,
                cloudinit = true
                );
            appliances.add(newAppliance);
            }
        }
        return appliances
    }
    override fun hasChanges(http: HttpClient): Boolean {
        return true;
    }
}