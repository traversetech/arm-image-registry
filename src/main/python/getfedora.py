#!/usr/bin/env python3

import fedfind.release
import json
import urllib
import requests
import sys

def get_compose_id(baseloc):
	compose_file = urllib.parse.urljoin(baseloc,"COMPOSE_ID")
	r = requests.get(compose_file)
	r.raise_for_status()
	return r.text

def get_latest_cloud_image_compose(releasenum):
	latest_path="https://kojipkgs.fedoraproject.org/compose/cloud/latest-Fedora-Cloud-{}/".format(releasenum)
	return get_compose_id(latest_path)

cloud_image_compose_id=get_latest_cloud_image_compose(sys.argv[1])
this_release = fedfind.release.get_release(dist="Fedora-Cloud",cid=cloud_image_compose_id)

selected_image = None
for image in this_release.all_images:
	if ("arch" not in image or image["arch"] != 'aarch64'):
		continue
	if (image["variant"] != "Cloud"):
		continue
	if ("subvariant" in image and image["subvariant"].find("UKI") > 0):
		continue
	if (image["format"] != "qcow2"):
		continue

	image["compose_id"] = cloud_image_compose_id
	selected_image = image
	break

print(json.dumps(selected_image))