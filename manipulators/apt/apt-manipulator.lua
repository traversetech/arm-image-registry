function do_manipulate(deploydev, deployroot, cconfigDict)
    local plfile = require "pl.file"
    local pldir = require "pl.dir"
    local plpath = require "pl.path"
    local cconfig = require "cconfig"
    
    local manipulator = require 'manipulator'

    local set_cmdline = "earlycon arm-smmu.disable_bypass=0 net.ifnames=0"

    pldir.makepath(deployroot)
    manipulator.expand_gpt(deploydev)

    print("Resizing rootfs and adding swap partition")
    manipulator.resize_rootfs_and_add_swap(deploydev, 1, deployroot, 4)

    print("Setting up target chroot")
    manipulator.setup_target_chroot(deploydev, 1, deployroot)

    print("Adding swap to fstab")
    manipulator.add_swap_fstab(deployroot, deploydev, 2)
    
    local os_release_data = manipulator.get_target_info(deployroot)
    local dist_id = os_release_data["ID"]
    
    print("Distribution is " .. dist_id)
    if (dist_id == "ubuntu") then
        local dist_ver = os_release_data["UBUNTU_CODENAME"]
        if (dist_ver == "mantic") then
            -- Ubuntu 23.10 and later have a separate /boot
            print("Mounting /boot")
            local boot_part = manipulator.get_partition_dev_for_device(deploydev, 16)
            manipulator.do_mount(boot_part, deployroot .. "/boot/", false)

            local efi_part = manipulator.get_partition_dev_for_device(deploydev, 15)
            manipulator.do_mount(efi_part, deployroot .. "/boot/efi", false)
        end

        print("Disabling flash-kernel")
        plfile.write(deployroot .. "/etc/flash-kernel/machine", "none")
    
        print("Running apt-get update")
        manipulator.run_command_in_target(deployroot, "apt-get update")

        print("Installing extra module package (DPAA2 drivers)")
        manipulator.run_command_in_target(deployroot,
                                          "FK_MACHINE=none apt-get -y install linux-image-extra-virtual")

        print("Removing secure boot shim")
        manipulator.run_command_in_target(deployroot, "apt-get --allow-remove-essential -y remove shim-signed grub-efi-arm64-signed")

        if (dist_ver == "mantic") then
            -- Fixes an issue with GRUB not being able to load the kernel image
            print("(Re)installing GRUB")
            manipulator.run_command_in_target(deployroot, "grub-install")
        end
    end

    print("Setting default kernel cmdline")
    manipulator.set_grub_cmdline(deployroot, set_cmdline)

    -- By default, Debian cloud image will hide the GRUB menu. Reverse this
    plfile.delete(deployroot.."etc/default/grub.d/15_timeout.cfg")
    -- For Ubuntu 21.10 and later
    if (plpath.exists(deployroot.."etc/default/grub.d/50-cloudimg-settings.cfg")) then
        plfile.delete(deployroot.."etc/default/grub.d/50-cloudimg-settings.cfg")
    end

    -- Workaround debian bug #955733 with cloud-init and locales
    -- https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=955733
    if (cconfigDict["locale"] ~= nil) then
        manipulator.enable_locales(deployroot, cconfigDict["locale"])
        manipulator.run_locale_gen(deployroot)
    end

    print("Running update-grub")
    manipulator.run_command_in_target(deployroot, "update-grub")

    local efipart = manipulator.get_partition_dev_for_device(deploydev, 15)
    manipulator.do_mount(efipart, deployroot .. "/boot/efi", false)

    local netconfig = nil
    if (cconfigDict["_netintf"] ~= nil) then
        print("Have network interface: " .. cconfigDict["_netintf"])
        local use_dhcpv6 = cconfigDict["_dhcpv6"] or false
        netconfig = cconfig.create_simple_network_config(cconfigDict["_netintf"], use_dhcpv6)
        cconfigDict["_netintf"] = nil
        cconfigDict["_dhcpv6"] = nil
    end

    if (dist_id == "ubuntu") then
        print("Making plain GRUB the default EFI application (disable SHIM)")
        plfile.delete(deployroot.."/boot/efi/EFI/BOOT/BOOTAA64.EFI")
        plfile.copy(deployroot.."/boot/efi/EFI/ubuntu/grubaa64.efi", deployroot.."/boot/efi/EFI/BOOT/BOOTAA64.EFI")
    end

    -- Debian cloud images will try to ifup every network interface before
    -- invoking cloud-init, which can significantly delay the boot process
    if (plpath.exists(deployroot .."/etc/udev/rules.d/75-cloud-ifupdown.rules") ~= nil) then
        plfile.delete(deployroot .."/etc/udev/rules.d/75-cloud-ifupdown.rules")
    end

    -- Fix Debian trying to find a resume partition on boot
    if (plpath.exists(deployroot .. "/etc/initramfs-tools/conf.d/") ~= nil) then
        plfile.write(deployroot .. "/etc/initramfs-tools/conf.d/resume.conf", "RESUME=none")
    end

    manipulator.create_embedded_nocloud_config(deployroot, cconfigDict, netconfig)
    manipulator.save_cconfig(deployroot .. "/boot/efi", cconfigDict, netconfig)

    manipulator.create_nocloud_config(deployroot, "UEFI")
    -- On Debian, the EFI partition doesn't have a label
    manipulator.label_fat_filesystem(deploydev, 15, "UEFI")

    print("Done!")
    manipulator.unmount_chroot(deployroot)
end
