# Manipulator scripts

Manipulator scripts are used to perform extra steps to the operating system
image after it has been written to a target disk.

Typical tasks include:

* Growing the partition table and main data partition up to the full disk size
* Creating a swap partition
* Downloading any extra drivers required for the target platform
* Changing the default kernel command line
* Placing a cloud-config file to be executed on first boot.

The manipulator library can be found in the [Ten64 recovery](https://gitlab.com/traversetech/ls1088firmware/recovery-ramdisk/-/blob/master/files/usr/lib/lua/manipulator.lua) project.

Currently there are two manipulators:
* 'APT' for Debian and Ubuntu
* 'Redhat' for Fedora and RHEL-based distributions such as CentOS

It is intended to have two different classes of manipulators:
* 'Fixup' to do hardware specific changes
* 'Provisioner' to do configuration changes such as placing cloud-config files

At the moment the two manipulators described here do both fixup and provisioning,
though chaining of manipulators with different roles is envisaged in the future.
