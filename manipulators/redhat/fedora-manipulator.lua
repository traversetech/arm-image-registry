function do_manipulate(deploydev, deployroot, cconfigDict)
    local plfile = require "pl.file"
    local pldir = require "pl.dir"
    local cconfig = require "cconfig"
    
    local manipulator = require 'manipulator'

    local set_cmdline = "earlycon arm-smmu.disable_bypass=0 net.ifnames=0 console=ttyS0,115200n8"

    if (manipulator.get_dev_type == nil) then
        error("ERROR: A newer recovery firmware is required, please see https://archive.traverse.com.au")
    end

    local FEDORA_ROOT_PART = 3 -- new setting after btrfs
    local FEDORA_EFI_PART = 1
    local FEDORA_BOOT_PART = 2

    pldir.makepath(deployroot)
    manipulator.expand_gpt(deploydev)

    print("Resizing rootfs and adding swap partition")
    local new_swap_part = manipulator.resize_rootfs_and_add_swap(deploydev, FEDORA_ROOT_PART, deployroot, 4)

    print("Setting up target chroot")
    manipulator.setup_target_chroot(deploydev, FEDORA_ROOT_PART, deployroot, "subvol=root")

    local os_release_data = manipulator.get_target_info(deployroot)
    local dist_id = os_release_data["ID"]
    
    print("Distribution is " .. dist_id)

    print("Adding swap to fstab")
    manipulator.add_swap_fstab(deployroot, deploydev, new_swap_part)

    -- We need to take note of the GRUB_CMDLINE_LINUX before we manipulate it
    -- so we can do a search and replace for blscfg
    local default_kernel_cmdline = manipulator.get_existing_cmdline(deployroot,"GRUB_CMDLINE_LINUX_DEFAULT")

    print("Setting default kernel cmdline")
    manipulator.set_grub_cmdline(deployroot, set_cmdline, "GRUB_CMDLINE_LINUX_DEFAULT")

    local bootpart = manipulator.get_partition_dev_for_device(deploydev, FEDORA_BOOT_PART)
    manipulator.do_mount(bootpart, deployroot .. "/boot", false)

    local efipart = manipulator.get_partition_dev_for_device(deploydev, FEDORA_EFI_PART)
    manipulator.do_mount(efipart, deployroot .. "/boot/efi", false)
    
    print("Running update-grub")
    manipulator.run_command_in_target(deployroot, "grub2-mkconfig -o /boot/efi/EFI/"..dist_id.."/grub.cfg")

    print("Updating blscfg cmdlines")
    manipulator.update_blscfg_cmdlines(deployroot, default_kernel_cmdline, set_cmdline)

    print("Making plain GRUB the default EFI application (disable SHIM)")
    manipulator.run_command_in_target(deployroot, "dnf install -y grub2-efi-aa64-modules")
    manipulator.run_command_in_target(deployroot, "dnf remove -y --setopt protected_packages= shim-aa64")
    manipulator.run_command_in_target(deployroot, "grub2-install --force --removable --no-nvram " .. deploydev)

    local netconfig = nil
    if (cconfigDict["_netintf"] ~= nil) then
        print("Have network interface: " .. cconfigDict["_netintf"])
        local use_dhcpv6 = cconfigDict["_dhcpv6"] or false
        netconfig = cconfig.create_simple_network_config(cconfigDict["_netintf"], use_dhcpv6)
        cconfigDict["_netintf"] = nil
        cconfigDict["_dhcpv6"] = nil
    end

    manipulator.create_embedded_nocloud_config(deployroot, cconfigDict, netconfig)

    print("Done!")
    manipulator.unmount_chroot(deployroot)

end