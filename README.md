# ARM64 Image Registry

This is a registry of OS images for arm64 that powers the muvirt appliancestore module, as well as the [Ten64 Bare Metal Appliance Store](https://ten64doc.traverse.com.au/software/appliancestore/).

The goal is to provide a convenient interface for deploying operating systems and application appliances
similar to the experience on some well known public cloud platforms.

Several categories are provided:
- *Official distribution images*: these are generally 'cloud' images published by
  major distributions.

  As some distributions refresh their cloud images every few weeks (to roll up new updates), the list is
  maintained by reading the distributions machine readable release files (where available)
  or by spidering the release folders to find new builds.

- *Traverse*: these images (generally) work as VMs but also have the right kernels and support packages
  to run bare metal on our boards, see our [distribution images](https://gitlab.com/traversetech/ls1088firmware/distribution-images).

- *Community*: for community submitted images.

All images *must* use EFI to boot - the only exceptions to this are bare metal operating systems for Traverse hardware which
are tied to the hardware capabilities (e.g routers using DPAA2 capabilities) or that are installed to the NAND flash.

### Manipulator scripts
This repository also houses "manipulator" scripts used by the [Ten64 recovery](https://gitlab.com/traversetech/ls1088firmware/recovery-ramdisk/)
firmware. Please see the [manipulators](manipulators/) folder for more information.

## Format
The format is pretty simple, but is subject to rapid change as new features are added.
```
{
    "id" : "debian-stable",
    "name" : "Debian stable (10)",
    "description" : "Debian stable (codename buster)",
    "maintainer" : "Debian",
    "download" : "https://cloud.debian.org/images/cloud/buster/20210329-591/debian-10-generic-arm64-20210329-591.qcow2",
    "link" : "https://cloud.debian.org/images/cloud/buster/",
    "checksum" : "sha512:cff9d5b59c748e986a74286365c0053ddbfc1ce919692bc11580cb4075dd95180cf0dd1c4773dc58a293778f3b3b548a52ab93211588cba43b6343848937cafc",
    "format" : "qcow2",
    "version" : "20210329-591",
    "mindisk" : 1024,
    "minram" : 1024,
    "supported_hardware" : null,
    "incompat_hardware" : null,
    "cloudinit" : true,
    "properties" : { }
},
```

The full list of parameters needed are:
- id: image id, typically distribution-release-variant
- name: Formal image name
- description: Image description - describe details about release (stable/unstable/beta/nightly), any default logins if the image does not use cloud-init, other variations
- link: link to more information about the image (usually the download page where the image is published)
- maintainer: maintainer of the image (not the entry itself!)
- download: download URL for the image file
- checksum: "sha256:..." or "sha512:..." 

  (note: If both are offered, take SHA256 > SHA512, as SHA256 can be accelerated on contemporary A53/A72 ARM64 CPUs with security extensions, SHA512 acceleration is only available on very new ~ARMv8.3 implementations)
- format (optional): qcow2 by default. Other formats include 'raw-xz'.
- version: image version string
- mindisk (optional): Minimum disk size needed for the image, in megabytes.
  This is used by the provisioning scripts in muvirt to create the correct volume size.
- supported_hardware: use to mark image that only works on a certain device
- incompat_hardware: use to mark devices which this image won't work on (can be overridden by manipulators)
- properties: machine dependent properties, for example:

   `x-dpaa2-legacy-ethernet`: use [legacy](https://ten64doc.traverse.com.au/network/managementmodes/) management mode for Gigabit Ethernet PHYs on Ten64

   `x-dpaa2-legacy-sfp`: use legacy management mode for SFP's on Ten64

  These are typically applied by the compatibility overlay (`state/compat.json`)

[JSON schema](https://json-schema.org/)'s are generated as part of the build pipeline, which can be used to validation.

# How to use this repository
The project uses [Maven](http://maven.apache.org/) to handle it's
build process.

You can use the `buildenv` container as a self-contained build environment.

There are several exec shortcuts defined in `pom.xml`:

```
mvn exec:java@generate-schema
mvn exec:java@generate-distribution-registry
mvn exec:java@generate-manipulator-registry
mvn exec:java@generate-traverse-registry
mvn exec:java@generate-community-registry
```

Please note: The Fedora scraper requires python3 and [fedfind](https://pypi.org/project/fedfind/)
to be installed.

# Technical Changelog
The initial generator prototype was written in Python using requests and luigi.

The generator code was rewritten from Python to Kotlin to take advantage of
the more rigid type-safety available in Kotlin, as well as the Jackson
databinding between JSON and Kotlin/Java objects.

## Submitting new images for inclusion
- If you are a distribution, please please please publish release data in a machine readable format.

  A good example is [Ubuntu](https://cloud-images.ubuntu.com/releases/streams/v1/com.ubuntu.cloud:released:download.json)'s list.
  Other potential formats are RSS etc. Ideally we want all the information described in the format above without having to
  download the actual image to generate.

- For new community images, open a pull request.
