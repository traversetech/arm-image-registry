# ARM Image Registry
# SPDX-License-Identifier:  GPL-2.0-or-later
from jsonpickle.unpickler import Unpickler

class ImageRegistryEntry:
    def __init__(self, newid):
        self.idstr = newid
        self.name = None
        self.description = None
        self.maintainer = None
        self.download = None
        self.link = None
        self.sha256sum = None
        self.format = "qcow2"
        self.version = None
        self.mindisk = None
        self.minram = None
        self.supported_hardware = []
        self.incompat_hardware = []
        self.cloudinit = True

    def add_supported_hardware(self, hardwareid):
        self.supported_hardware.append(hardwareid)

    def add_incompat_hardware(self, hardwareid):
        self.incompat_hardware.append(hardwareid)

# Deserialize an entry from a JSON dictionary
# TODO: Do some sort of schema validation
def deserialize_image_entry(jsondict):
    u = Unpickler()
    jsondict["py/object"] = "imageregistry.entry.ImageRegistryEntry"
    jsondict["idstr"] = jsondict["id"]

    entry = u.restore(jsondict)

    return entry