# ARM Image Registry
# SPDX-License-Identifier:  GPL-2.0-or-later
from datetime import datetime
class ImageRegistryRepository:
    appliances = None
    updated = None

    def __init__(self):
        self.appliances = []

    def set_appliances(self, applist):
        self.appliances = applist

    def set_updated(self, updtime):
        self.updated = updtime

    def set_updated_now(self):
        nowtime = datetime.now()
        self.updated = nowtime.strftime('%Y-%m-%dT%H:%M:%SZ')
