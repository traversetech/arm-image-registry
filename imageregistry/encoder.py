from imageregistry.entry import ImageRegistryEntry
from imageregistry.repository import ImageRegistryRepository
from json import JSONEncoder

class ImageRegistryEncoder(JSONEncoder):
    def default(self, obj):
        if(isinstance(obj, ImageRegistryEntry)):
            selfdict = obj.__dict__
            selfdict["id"] = obj.idstr
            del selfdict["idstr"]
            return selfdict
        elif(isinstance(obj, ImageRegistryRepository)):
            selfdict = obj.__dict__
            return selfdict
        return JSONEncoder.default(self, obj)
